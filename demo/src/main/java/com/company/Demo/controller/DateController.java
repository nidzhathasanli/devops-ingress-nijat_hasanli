package com.company.Demo.controller;

import com.company.Demo.model.DateDto;
import com.company.Demo.service.DateService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/date")
@RequiredArgsConstructor
public class DateController {

    private final DateService dateService;

    @GetMapping
    public ResponseEntity<DateDto> getDate() {
        return ResponseEntity.ok(dateService.getDate());
    }

}

package com.company.Demo.model;

import java.time.LocalDateTime;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class DateDto {

    private String env;
    private LocalDateTime date;
}

FROM maven:3.6.3-jdk-11
COPY demo/target/Demo-0.0.1-SNAPSHOT.jar app.jar
CMD ["java","-jar","app.jar"]
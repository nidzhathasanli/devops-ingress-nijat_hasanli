from http.server import BaseHTTPRequestHandler, HTTPServer
from datetime import datetime

class SimpleHTTPRequestHandler(BaseHTTPRequestHandler):
    def _set_response(self, content_type="text/plain"):
        self.send_response(200)
        self.send_header('Content-type', content_type)
        self.end_headers()

    def do_GET(self):
        if self.path == '/date':
            self._set_response("text/plain")
            now = datetime.now()
            current_time = now.strftime("%Y-%m-%d %H:%M:%S")
            seconds = now.second
            if seconds % 2 == 0:
                current_time += " even"
            else:
                current_time += " odd"
            self.wfile.write(current_time.encode())
        else:
            self._set_response("text/plain")
            self.wfile.write("Invalid path".encode())

def run(server_class=HTTPServer, handler_class=SimpleHTTPRequestHandler, port=80):
    server_address = ('', port)
    httpd = server_class(server_address, handler_class)
    print(f"Starting server on port {port}...")
    httpd.serve_forever()

if __name__ == '__main__':
    run()

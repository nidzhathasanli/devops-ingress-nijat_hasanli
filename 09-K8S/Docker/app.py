import os
from flask import Flask

app = Flask(__name__)

@app.route('/color')
def get_color():
    return os.environ.get('APP_COLOR', 'default')

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8080)
